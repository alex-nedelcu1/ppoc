package com.example;

import com.example.calculator.Calculator;
import com.example.calculator.ICalculator;
import com.example.exceptions.InvalidCalculatorUsageException;
import com.example.expression_helpers.cleaner.ExpressionCleaner;
import com.example.expression_helpers.cleaner.IExpressionCleaner;
import com.example.expression_helpers.parser.ExpressionParser;
import com.example.expression_helpers.parser.IExpressionParser;
import com.example.expression_helpers.validator.ExpressionValidator;
import com.example.expression_helpers.validator.IExpressionValidator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        IExpressionCleaner expressionCleaner = new ExpressionCleaner();

        IExpressionValidator expressionValidator = new ExpressionValidator();

        IExpressionParser expressionParser = new ExpressionParser();

        ICalculator calculator = new Calculator(expressionCleaner, expressionValidator, expressionParser);

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("Expression (q to quit): ");
            String expression = scanner.nextLine();
            if (expression.equals("q")) {
                break;
            }
            try {
                System.out.println("Result is: " + calculator.compute(expression));
            } catch (InvalidCalculatorUsageException exception) {
                System.out.println("Invalid calculator usage exception occurred: " + exception.getMessage());
            } catch (ArithmeticException exception) {
                System.out.println("Arithmetic exception occurred! Please note that the calculator works only with integer values.");
            } finally {
                System.out.println();
            }
        }
    }
}
