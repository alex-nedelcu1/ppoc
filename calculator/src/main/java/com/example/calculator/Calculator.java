package com.example.calculator;

import com.example.enums.Operator;
import com.example.exceptions.InvalidCalculatorUsageException;
import com.example.expression_helpers.cleaner.IExpressionCleaner;
import com.example.expression_helpers.parser.IExpressionParser;
import com.example.expression_helpers.validator.IExpressionValidator;
import com.example.factor.Factor;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.example.enums.Operator.*;


@RequiredArgsConstructor
public class Calculator implements ICalculator {

    private final IExpressionCleaner expressionCleaner;

    private final IExpressionValidator expressionValidator;

    private final IExpressionParser expressionParser;

    @Override
    public Integer compute(final String expression) {
        String cleanedExpression = expressionCleaner.clean(expression);

        if (!expressionValidator.isExpressionValid(cleanedExpression)) {
            throw new InvalidCalculatorUsageException("Invalid expression!");
        }

        var signedFirstOrderExpressions = expressionParser.divideBySecondOrderOperators(cleanedExpression);

        var finalResult = signedFirstOrderExpressions.stream()
                .map(this::computeFirstOrderExpression)
                .reduce(0, Integer::sum);

        return finalResult;
    }

    private Integer computeFirstOrderExpression(final Pair<Operator, String> signedExpression) {
        var sign = signedExpression.getLeft();
        var expression = signedExpression.getRight();

        return expressionParser.isFirstOrderExpressionComposed(expression)
                ? applySign(computeComposedFirstOrderExpression(signedExpression), sign)
                : applySign(computeSimpleFirstOrderExpression(signedExpression), sign);
    }

    private Integer applySign(Integer unsigned, Operator sign) {
        if (sign == null) {
            throw new InvalidCalculatorUsageException("Invalid expression!");
        }

        return sign.equals(PLUS) ? unsigned : -unsigned;
    }

    /**
     * Computes the result of a composed expression
     *
     * @param composedSignedExpression expression that contains multiplications or divisions
     * @return result of the expression
     */
    private Integer computeComposedFirstOrderExpression(final Pair<Operator, String> composedSignedExpression) {
        var sign = composedSignedExpression.getLeft();
        var composedExpression = composedSignedExpression.getRight();

        List<Integer> finalSimpleFactors = expressionParser.divideByFirstOrderOperators(composedExpression)
                .stream()
                .map(simpleExpression -> computeSimpleFirstOrderExpression(Pair.of(sign, simpleExpression)))
                .collect(Collectors.toList());

        List<Operator> firstOrderOperators = expressionParser.extractFirstOrderOperatorsInOrder(composedExpression);

        return applyOperatorsToFactors(finalSimpleFactors, firstOrderOperators);
    }

    /**
     * Computes the result of a simple expression.
     * A simple expression can be: integer, sqrt(integer)
     *
     * @param simpleSignedExpression expression that does not contain further multiplications or divisions
     * @return result of the expression
     */
    private Integer computeSimpleFirstOrderExpression(final Pair<Operator, String> simpleSignedExpression) {
        var factor = simpleSignedExpression.getRight();
        var type = expressionParser.getFactorType(factor);

        return new Factor(factor, type).resolve();
    }

    private Integer applyOperatorsToFactors(List<Integer> factors, List<Operator> operators) {
        AtomicReference<Integer> result = new AtomicReference<>(factors.get(0));
        var slicedFactors = factors.stream().skip(1).collect(Collectors.toList());

        IntStream.range(0, slicedFactors.size()).forEach(index -> {
            var factor = slicedFactors.get(index);
            var operator = operators.get(index);

            if (operator.equals(MULTIPLY)) {
                result.set(result.get() * factor);
            } else if (operator.equals(DIVIDE)) {
                if (factor.equals(0)) {
                    throw new InvalidCalculatorUsageException("Division by 0");
                }

                result.set(result.get() / factor);
            }
        });

        return result.get();
    }
}
