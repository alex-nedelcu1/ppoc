package com.example.calculator;

public interface ICalculator {

    Integer compute(String expression);

}
