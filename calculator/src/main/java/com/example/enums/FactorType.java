package com.example.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum FactorType {
    SQRT("sqrt"),
    MIN("min"),
    MAX("max"),
    SIMPLE("");

    @Getter
    private final String type;
}
