package com.example.exceptions;

public class InvalidCalculatorUsageException extends RuntimeException {

    public InvalidCalculatorUsageException(String message) {
        super(message);
    }

}
