package com.example.expression_helpers.parser;

import com.example.enums.FactorType;
import com.example.enums.Operator;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

public interface IExpressionParser {

    List<Pair<Operator, String>> divideBySecondOrderOperators(String expression);

    List<String> divideByFirstOrderOperators(String expression);

    List<Operator> extractFirstOrderOperatorsInOrder(String expression);

    FactorType getFactorType(String factor);

    boolean isFirstOrderExpressionComposed(String expression);

}
