package com.example.expression_helpers.validator;

public interface IExpressionValidator {

    boolean isExpressionValid(String expression);

}
