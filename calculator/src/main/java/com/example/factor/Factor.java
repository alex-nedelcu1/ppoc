package com.example.factor;

import com.example.enums.FactorType;
import com.example.exceptions.InvalidCalculatorUsageException;
import com.google.common.math.IntMath;
import lombok.Builder;
import lombok.RequiredArgsConstructor;

import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Builder
public class Factor implements IFactor {

    private final String inner;

    private final FactorType type;


    @Override
    public Integer resolve() {
        Integer result;

        switch (type) {
            case SIMPLE:
                if (inner.isEmpty()) {
                    throw new InvalidCalculatorUsageException("Invalid expression!");
                }

                result = Integer.parseInt(inner);
                break;
            case SQRT:
                var number = inner.replaceAll("[^0-9]+", "");

                if (number.isEmpty()) {
                    throw new InvalidCalculatorUsageException("Invalid expression!");
                }

                result = IntMath.sqrt(Integer.parseInt(number), RoundingMode.UNNECESSARY);
                break;
            case MIN:
                String[] minNumberTokens = inner.replaceAll("[^0-9]+", " ").split(" ");

                if (minNumberTokens.length == 0) {
                    throw new InvalidCalculatorUsageException("Invalid expression!");
                }

                var reversed = Arrays.stream(minNumberTokens)
                        .filter(str -> str.trim().length() > 0)
                        .map(Integer::parseInt)
                        .sorted(Comparator.naturalOrder())
                        .collect(Collectors.toList());
                result = reversed.get(0);
                break;
            case MAX:
                String[] maxNumberTokens = inner.replaceAll("[^0-9]+", " ").split(" ");

                if (maxNumberTokens.length == 0) {
                    throw new InvalidCalculatorUsageException("Invalid expression!");
                }

                var sorted = Arrays.stream(maxNumberTokens)
                        .filter(str -> str.trim().length() > 0)
                        .map(Integer::parseInt)
                        .sorted(Comparator.reverseOrder())
                        .collect(Collectors.toList());
                result = sorted.get(0);
                break;
            default:
                throw new IllegalStateException("Unexpected type: " + type.getType());
        }

        return result;
    }

}
