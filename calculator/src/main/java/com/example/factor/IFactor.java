package com.example.factor;

public interface IFactor {

    Integer resolve();

}
