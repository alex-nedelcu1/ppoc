package com.example.expression_helpers;

import com.example.expression_helpers.cleaner.ExpressionCleaner;
import com.example.expression_helpers.cleaner.IExpressionCleaner;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ExpressionCleanerTest {

    private final IExpressionCleaner expressionCleaner = new ExpressionCleaner();

    private static Stream<Arguments> provideExpressions() {
        return Stream.of(
                Arguments.of("2  + 4  / 1", "+2+4/1"),
                Arguments.of("MIN(11,4) -   SqRt(4) + 50   ", "+min(11,4)-sqrt(4)+50"),
                Arguments.of("   ", null),
                Arguments.of(null, null)
        );
    }

    @ParameterizedTest
    @MethodSource("provideExpressions")
    public void shouldCleanExpressions(String expression, String expectedAfterClean) {
        assertEquals(expectedAfterClean, expressionCleaner.clean(expression));
    }
}
