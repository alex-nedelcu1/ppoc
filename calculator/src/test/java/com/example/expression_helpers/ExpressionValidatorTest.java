package com.example.expression_helpers;

import com.example.expression_helpers.validator.ExpressionValidator;
import com.example.expression_helpers.validator.IExpressionValidator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExpressionValidatorTest {

    private final IExpressionValidator expressionValidator = new ExpressionValidator();

    @ParameterizedTest
    @ValueSource(strings = {"2+3+4", "sqrt(64)*9/3  +3 -11 /1/11 *5", "2", "123*123/sqrt(123)", "2///1++5"})
    public void shouldReturnTrueForValidExpressions(String validExpression) {
        assertTrue(expressionValidator.isExpressionValid(validExpression));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"a", "45+1/3$3", "123.34 +54 /2"})
    public void shouldReturnFalseForInvalidExpressions(String invalidExpression) {
        assertFalse(expressionValidator.isExpressionValid(invalidExpression));
    }
}
