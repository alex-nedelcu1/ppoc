package com.tora.rabbitmq_broadcast;

import com.tora.rabbitmq_broadcast.publisher.IPublisher;
import com.tora.rabbitmq_broadcast.publisher.Publisher;
import com.tora.rabbitmq_broadcast.subscriber.ISubscriber;
import com.tora.rabbitmq_broadcast.subscriber.Subscriber;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public final class Main {

  // before executing run docker image
  // docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.11-management

  public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
    IPublisher publisher = new Publisher();
    ISubscriber subscriber1 = new Subscriber();
    ISubscriber subscriber2 = new Subscriber();
    ISubscriber subscriber3 = new Subscriber();
    ISubscriber subscriber4 = new Subscriber();

    publisher.publish("Will be ignored"); // will not be received by anyone

    Thread.sleep(500L);

    subscriber1.consume();
    subscriber2.consume();
    subscriber3.consume();

    Thread.sleep(500L);

    publisher.publish("Will be processed 3 times"); // will be received by subscribers 1, 2, 3

    Thread.sleep(500L);

    subscriber4.consume();

    Thread.sleep(500L);

    publisher.publish("Will be processed 4 times"); // will be received by subscribers 1, 2, 3
  }
}