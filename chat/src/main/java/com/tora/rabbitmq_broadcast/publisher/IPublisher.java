package com.tora.rabbitmq_broadcast.publisher;

public interface IPublisher {

  void publish(String message);

}
