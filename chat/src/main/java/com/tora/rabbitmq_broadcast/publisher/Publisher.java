package com.tora.rabbitmq_broadcast.publisher;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Publisher implements IPublisher {

  private static final String EXCHANGE_NAME = "broadcast-exchange";

  @Override
  public void publish(String message) {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(ConnectionFactory.DEFAULT_HOST); // connect to local RabbitMQ node

    try (
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel()
    ) {
      // fanout exchange (exchange ~ buffer between publisher and queues): broadcasts received messages to all the known queues
      channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);

      // publish in the exchange that will broadcast (due to fanout mode) to all known queues
      channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes());

      System.out.println(" [x] Sent '" + message + "'");
    } catch (IOException | TimeoutException e) {
      throw new RuntimeException(e);
    }
  }
}
