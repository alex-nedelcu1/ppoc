package com.tora.rabbitmq_broadcast.subscriber;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public interface ISubscriber {

  void consume() throws IOException, TimeoutException;

}
