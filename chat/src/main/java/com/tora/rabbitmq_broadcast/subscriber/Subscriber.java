package com.tora.rabbitmq_broadcast.subscriber;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import lombok.Getter;
import org.junit.platform.commons.util.StringUtils;

public class Subscriber implements ISubscriber {

  private static final String EXCHANGE_NAME = "broadcast-exchange";
  @Getter
  private final List<String> deliveries;

  public Subscriber() {
    this.deliveries = new ArrayList<>();
  }

  @Override
  public void consume() throws IOException, TimeoutException {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost(ConnectionFactory.DEFAULT_HOST);

    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);

    // create a non-durable, exclusive, auto-delete queue with a generated name
    String queueName = channel.queueDeclare().getQueue();

    // link the queue with the exchange such that subscriber receives broadcast messages
    channel.queueBind(queueName, EXCHANGE_NAME, "");

    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

    DeliverCallback deliverCallback = (consumerTag, delivery) -> {
      String receivedMessage = new String(delivery.getBody(), StandardCharsets.UTF_8);
      System.out.println(" [x] Received '" + receivedMessage + "'");

      try {
        processDelivery(receivedMessage);
        System.out.println(" [x]  -- Processing '" + receivedMessage + "' done");
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    };

    boolean autoAck = true;
    channel.basicConsume(queueName, autoAck, deliverCallback, (consumerTag) -> {
    });
  }

  private void processDelivery(String delivery) throws InterruptedException {
    Thread.sleep(500L);

    if (!StringUtils.isBlank(delivery)) {
      deliveries.add(delivery.trim().toLowerCase());
    }
  }
}
