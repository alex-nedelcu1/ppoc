package com.tora.socket_server_p2p_chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client implements Runnable {

  private Socket client;
  private BufferedReader reader; // read message from server
  private PrintWriter writer; // send message to server
  private boolean done;

  @Override
  public void run() {
    // Thread that reads messages from server and displays them

    try {
      client = new Socket(Configuration.HOST, Configuration.PORT);
      writer = new PrintWriter(client.getOutputStream(), true);
      reader = new BufferedReader(new InputStreamReader(client.getInputStream()));

      InputHandler inputHandler = new InputHandler();
      Thread inputHandlerThread = new Thread(inputHandler);
      inputHandlerThread.start();

      String serverMessage;
      while (!done && (serverMessage = reader.readLine()) != null) {
        System.out.println(serverMessage);
      }
    } catch (IOException error) {
      throw new RuntimeException("Exception in client messages receiver: " + error);
    } finally {
      shutdown();
    }
  }

  public void shutdown() {
    done = true;
    try {
      reader.close();
      writer.close();
      if (!client.isClosed()) {
        client.close();
      }
    } catch (IOException error) {
      throw new RuntimeException(error);
    }
  }

  class InputHandler implements Runnable {
    // Thread that inputs messages from keyboard and sends them to server

    @Override
    public void run() {
      try {
        // Read input from keyboard
        BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));

        while (!done) {
          String message = inputReader.readLine();

          if (isQuitCommand(message)) {
            // Send quit command to server
            writer.println(message);
            inputReader.close();
            shutdown();
          } else {
            writer.println(message);
          }
        }
      } catch (IOException error) {
        error.printStackTrace();
        shutdown();
      }
    }

    private boolean isQuitCommand(String message) {
      return message != null && message.trim().startsWith("/quit");
    }
  }

  public static void main(String[] args) {
    Client client = new Client();
    client.run();
  }
}
