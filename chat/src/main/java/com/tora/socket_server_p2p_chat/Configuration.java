package com.tora.socket_server_p2p_chat;

public class Configuration {
  public static final int PORT = 9999;
  public static final String HOST = "127.0.0.1";
}
