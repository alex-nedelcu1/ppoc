package com.tora.socket_server_p2p_chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Troubleshooting for: "Cannot invoke "java.net.ServerSocket.close()" because "this.server" is null"
 * <p>
 * Solution: find process running on port 9999: `lsof -i tcp:9999` kill process with PID x: `kill -9 x`
 */

public class Server implements Runnable {

  private ExecutorService executor;
  private final List<ConnectionHandler> connections;
  private ServerSocket server;
  private boolean done;

  public Server() {
    this.connections = new ArrayList<>();
    this.done = false;
  }

  @Override
  public void run() {
    try {
      executor = Executors.newCachedThreadPool();
      server = new ServerSocket(Configuration.PORT);
      System.out.println("Server is waiting for client connections...");

      while (!done) {
        Socket client = server.accept();
        System.out.println("Accepted new client connection: " + client.getRemoteSocketAddress());

        // Delegate client connection to separate thread
        ConnectionHandler connection = new ConnectionHandler(client);
        connections.add(connection);
        executor.execute(connection);
      }
    } catch (Exception error) {
      throw new RuntimeException("Exception in server main: " + error);
    } finally {
      shutdown();
    }
  }

  private void shutdown() {
    try {
      done = true;
      server.close();
      executor.shutdown();
      shutdownConnections();
    } catch (IOException error) {
      error.printStackTrace();
    }
  }

  private void shutdownConnections() {
    for (ConnectionHandler connection : connections) {
      connection.shutdownConnection();
    }
  }

  public void broadcast(String message) {
    for (ConnectionHandler connection : connections) {
      if (connection != null) {
        connection.sendMessage("[SERVER] " + message);
      }
    }
  }

  class ConnectionHandler implements Runnable {

    private final Socket client;
    private String nickname;
    private BufferedReader reader; // read message from client
    private PrintWriter writer; // send message to client
    private boolean stopConnection;

    private ConnectionHandler(Socket client) {
      this.client = client;
      this.stopConnection = false;
    }

    @Override
    public void run() {
      try {
        writer = new PrintWriter(client.getOutputStream(), true);
        reader = new BufferedReader(new InputStreamReader(client.getInputStream()));

        getClientNickname();
        System.out.println("Client " + client.getRemoteSocketAddress() + " set their nickname to '" + nickname + "'");
        broadcast("Hi everyone, please welcome '" + nickname + "' who has just joined the chat!");

        String clientMessage;

        while (!stopConnection && (clientMessage = reader.readLine()) != null) {
          if (isQuitCommand(clientMessage)) {
            broadcast(nickname + " left the chat!");
            shutdownConnection();
          } else {
            broadcast(nickname + ": " + clientMessage);
          }
        }
      } catch (IOException error) {
        shutdownConnection();
        error.printStackTrace();
      }
    }

    private void getClientNickname() {
      boolean valid = false;

      while (!valid) {
        try {
          writer.println("Please enter your nickname below:");
          nickname = reader.readLine().trim();

          if (nickname != null && !nickname.isEmpty()) {
            valid = true;
            writer.println("You have successfully set your nickname to " + nickname);
          } else {
            writer.println("Invalid nickname, please try another one.");
          }
        } catch (IOException error) {
          throw new RuntimeException("Exception while setting client nickname: " + error);
        }
      }
    }

    private boolean isQuitCommand(String message) {
      return message != null && message.trim().startsWith("/quit");
    }

    public void sendMessage(String message) {

      writer.println(message);
    }

    public void shutdownConnection() {
      try {
        stopConnection = true;
        reader.close();
        writer.close();
        if (!client.isClosed()) {
          client.close();
        }
      } catch (IOException error) {
        error.printStackTrace();
      }
    }
  }

  public static void main(String[] args) {
    Server server = new Server();
    server.run();
  }
}
