package com.example;

import com.example.repositories.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 2, timeUnit = TimeUnit.SECONDS, batchSize = 100)
@Threads(4)
public class BenchmarkRepository {
    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder().include(BenchmarkRepository.class.getSimpleName()).forks(1).build();

        new Runner(options).run();
    }

    @Benchmark
    public void addArrayList(ArrayListState state, OrderState order) {
        state.repository.add(order.order);
    }

    @Benchmark
    public void addObject2ObjectAVLTreeMap(Object2ObjectAVLTreeMapState state, OrderState order) {
        state.repository.add(order.order);
    }

    @Benchmark
    public void addConcurrentHashMap(ConcurrentHashMapState state, OrderState order) {
        state.repository.add(order.order);
    }

    @Benchmark
    public void addHashSet(HashSetState state, OrderState order) {
        state.repository.add(order.order);
    }

    @Benchmark
    public void addTreeSortedMap(TreeSortedMapState state, OrderState order) {
        state.repository.add(order.order);
    }

    @Benchmark
    public void addTreeSet(TreeSetState state, OrderState order) {
        state.repository.add(order.order);
    }

    // add

    @Benchmark
    public void containsArrayList(ArrayListState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.contains(order.order);
    }

    @Benchmark
    public void containsObject2ObjectAVLTreeMap(Object2ObjectAVLTreeMapState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.contains(order.order);
    }

    @Benchmark
    public void containsConcurrentHashMap(ConcurrentHashMapState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.contains(order.order);
    }

    @Benchmark
    public void containsHashSet(HashSetState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.contains(order.order);
    }

    @Benchmark
    public void containsTreeSortedMap(TreeSortedMapState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.contains(order.order);
    }

    @Benchmark
    public void containsTreeSet(TreeSetState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.contains(order.order);
    }

    // contains

    @Benchmark
    public void removeArrayList(ArrayListState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.remove(order.order);
    }

    @Benchmark
    public void removeObject2ObjectAVLTreeMap(Object2ObjectAVLTreeMapState state, OrderState order) {
        state.repository.remove(order.order);
    }

    @Benchmark
    public void removeConcurrentHashMap(ConcurrentHashMapState state, OrderState order) {
        state.repository.remove(order.order);
    }

    @Benchmark
    public void removeHashSet(HashSetState state, OrderState order) {
        state.repository.remove(order.order);
    }

    @Benchmark
    public void removeTreeSortedMap(TreeSortedMapState state, OrderState order) {
        state.repository.remove(order.order);
    }

    @Benchmark
    public void removeTreeSet(TreeSetState state, OrderState order) {
        state.repository.remove(order.order);
    }

    // remove

    @State(Scope.Benchmark)
    public static class OrderState {
        Order order = new Order(1, 2, 3);
    }

    @State(Scope.Benchmark)
    public static class TreeSetState {
        TreeSetBasedRepository<Order> repository = new TreeSetBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class ArrayListState {
        ArrayListBasedRepository<Order> repository = new ArrayListBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class Object2ObjectAVLTreeMapState {
        Object2ObjectAVLTreeMapBasedRepository<Integer, Order> repository = new Object2ObjectAVLTreeMapBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class ConcurrentHashMapState {
        ConcurrentHashMapBasedRepository<Integer, Order> repository = new ConcurrentHashMapBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class HashSetState {
        HashSetBasedRepository<Order> repository = new HashSetBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class TreeSortedMapState {
        TreeSortedMapBasedRepository<Integer, Order> repository = new TreeSortedMapBasedRepository<>();
    }
}
