package com.example;

public interface Identifiable<ID> {

    ID getId();

}
