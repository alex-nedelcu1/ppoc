package com.example;

import java.util.Objects;

public class Order implements Comparable<Order>, Identifiable<Integer> {

    private final Integer id;
    private int price;
    private int quantity;


    public Order(Integer id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }

        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        Order order = (Order) object;

        return Objects.equals(id, order.id);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public int compareTo(Order order) {
        return Integer.compare(this.price, order.price);
    }

    @Override
    public Integer getId() {
        return id;
    }
}
