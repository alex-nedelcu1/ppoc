package com.example.repositories;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {

    private final List<T> list;

    public ArrayListBasedRepository() {
        this.list = new ArrayList<>();
    }

    @Override
    public void add(T entity) {
        synchronized (this) {
            list.add(entity);
        }
    }

    @Override
    public boolean contains(T entity) {
        synchronized (this) {
            return list.contains(entity);
        }
    }

    @Override
    public void remove(T entity) {
        synchronized (this) {
            list.remove(entity);
        }
    }
}
