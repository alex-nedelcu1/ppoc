package com.example.repositories;

import com.example.Identifiable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<ID, T extends Identifiable<ID>> implements InMemoryRepository<T> {

    private final Map<ID, T> map;

    public ConcurrentHashMapBasedRepository() {
        this.map = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T entity) {
        map.put(entity.getId(), entity);
    }

    @Override
    public boolean contains(T entity) {
        return map.containsKey(entity.getId());
    }

    @Override
    public void remove(T entity) {
        map.remove(entity.getId());
    }
}
