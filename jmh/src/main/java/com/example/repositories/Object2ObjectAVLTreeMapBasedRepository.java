package com.example.repositories;

import com.example.Identifiable;
import it.unimi.dsi.fastutil.objects.Object2ObjectAVLTreeMap;

import java.util.Map;

// Fastutil
public class Object2ObjectAVLTreeMapBasedRepository<ID, T extends Identifiable<ID>> implements InMemoryRepository<T> {

    private final Map<ID, T> map;

    public Object2ObjectAVLTreeMapBasedRepository() {
        this.map = new Object2ObjectAVLTreeMap<>();
    }

    @Override
    public void add(T entity) {
        map.put(entity.getId(), entity);
    }

    @Override
    public boolean contains(T entity) {
        return map.containsKey(entity.getId());
    }

    @Override
    public void remove(T entity) {
        map.remove(entity.getId());
    }
}
