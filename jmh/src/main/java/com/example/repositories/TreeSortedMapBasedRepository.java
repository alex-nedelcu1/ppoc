package com.example.repositories;


import com.example.Identifiable;
import org.eclipse.collections.impl.map.sorted.mutable.TreeSortedMap;

import java.util.Map;

// Eclipse
public class TreeSortedMapBasedRepository<ID, T extends Identifiable<ID>> implements InMemoryRepository<T> {

    private final Map<ID, T> map;

    public TreeSortedMapBasedRepository() {
        this.map = new TreeSortedMap<>();
    }

    @Override
    public void add(T entity) {
        map.put(entity.getId(), entity);
    }

    @Override
    public boolean contains(T entity) {
        return map.containsKey(entity.getId());
    }

    @Override
    public void remove(T entity) {
        map.remove(entity.getId());
    }
}
