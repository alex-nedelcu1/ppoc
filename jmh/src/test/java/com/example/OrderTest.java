package com.example;

import junit.framework.TestCase;
import org.junit.Before;

public class OrderTest extends TestCase {

    private Order order;
    private Order other;
    private Order different;

    @Before
    public void setUp() {
        order = new Order(1, 2, 3);
        other = new Order(1, 505, 800);
        different = new Order(2, 505, 800);
    }

    public void testSetPrice() {
        assertEquals(2, order.getPrice());
        order.setPrice(100);
        assertEquals(100, order.getPrice());
    }

    public void testSetQuantity() {
        assertEquals(3, order.getQuantity());
        order.setQuantity(500);
        assertEquals(500, order.getQuantity());
    }

    public void testGetPrice() {
        assertEquals(2, order.getPrice());
    }

    public void testGetQuantity() {
        assertEquals(3, order.getQuantity());
    }

    public void testTestEquals() {
        assertTrue(order.equals(other));
        assertTrue(order.equals(order));
        assertFalse(order.equals(different));
        assertFalse(order.equals(null));
    }

    public void testTestHashCode() {
        assertEquals(order.hashCode(), other.hashCode());
        assertFalse(order.hashCode() == different.hashCode());
    }

    public void testCompareTo() {
        assertEquals(-1, order.compareTo(other));
    }

    public void testGetId() {
        assertEquals(Integer.valueOf(1), order.getId());
    }

    public void testToString() {
        String expectedToString = "Order{" +
                "id=" + order.getId() +
                ", price=" + order.getPrice() +
                ", quantity=" + order.getQuantity() +
                '}';

        assertEquals(expectedToString, order.toString());
    }
}
