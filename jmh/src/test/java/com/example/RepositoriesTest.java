package com.example;

import com.example.repositories.*;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RepositoriesTest {

    @Test
    public void shouldAddContainRemoveNotContain() {
        Order first = new Order(1, 2, 3);
        Order second = new Order(2, 20, 30);


        for (int i = 0; i <= 5; i += 1) {
            InMemoryRepository repository = getRepository(i);

            assertFalse(repository.contains(first));
            assertFalse(repository.contains(second));

            repository.add(first);
            repository.add(second);

            assertTrue(repository.contains(first));
            assertTrue(repository.contains(second));

            repository.remove(first);
            assertFalse(repository.contains(first));
            assertTrue(repository.contains(second));

            repository.remove(second);
            assertFalse(repository.contains(first));
            assertFalse(repository.contains(second));
        }
    }

    private <ID, T extends Identifiable<ID>, R extends Comparable<R>> InMemoryRepository getRepository(int type) {
        switch (type) {
            case 0:
                return new ArrayListBasedRepository<>();
            case 1:
                return new ConcurrentHashMapBasedRepository<>();
            case 2:
                return new HashSetBasedRepository<>();
            case 3:
                return new Object2ObjectAVLTreeMapBasedRepository<>();
            case 4:
                return new TreeSetBasedRepository<R>();
            case 5:
                return new TreeSortedMapBasedRepository<>();
            default:
                throw new RuntimeException();
        }
    }
}
