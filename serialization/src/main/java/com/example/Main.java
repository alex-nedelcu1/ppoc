package com.example;

import com.example.implementations.BigDecimalCalculator;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        List<BigDecimal> values = IntStream.range(0, 10_000_000)
                .mapToObj(BigDecimal::new)
                .collect(Collectors.toList());

        BigDecimalCalculator bigDecimalCalculator = new BigDecimalCalculator();

        System.out.println(bigDecimalCalculator.getSum(values));
        System.out.println(bigDecimalCalculator.getAverage(values));
        System.out.println(bigDecimalCalculator.getTopPercentValues(values, 0.000001));
    }
}
