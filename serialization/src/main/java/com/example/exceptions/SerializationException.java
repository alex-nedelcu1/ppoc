package com.example.exceptions;

public class SerializationException extends RuntimeException {

    public SerializationException(String message) {
        super(message);
    }
}
