package com.example.implementations;

import com.example.interfaces.IBigDecimalCalculator;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BigDecimalCalculator implements IBigDecimalCalculator {

    @Override
    public BigDecimal getSum(List<BigDecimal> values) {
        return values.parallelStream()
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    @Override
    public BigDecimal getAverage(List<BigDecimal> values) {
        if (values.size() == 0) {
            return null;
        }

        BigDecimal sum = getSum(values);
        BigDecimal howMany = new BigDecimal(values.size());

        return sum.divide(howMany);
    }

    @Override
    public List<BigDecimal> getTopPercentValues(List<BigDecimal> values, double percent) {
        long limit = convertPercentToLimit(percent, values.size());

        return values.parallelStream()
                .sorted(Comparator.reverseOrder())
                .limit(limit)
                .collect(Collectors.toList());
    }

    @Override
    public Runnable getLambdaForSum(List<BigDecimal> values) {
        Runnable lambdaForSum = (Serializable & Runnable) () -> System.out.println(getSum(values));
        return lambdaForSum;
    }

    @Override
    public Runnable getLambdaForAverage(List<BigDecimal> values) {
        Runnable lambdaForAverage = (Serializable & Runnable) () -> System.out.println(getAverage(values));
        return lambdaForAverage;
    }

    @Override
    public Runnable getLambdaForTopPercentValues(List<BigDecimal> values, double percent) {
        Runnable lambdaForTopPercentValues = (Serializable & Runnable) () -> System.out.println(getTopPercentValues(values, percent));
        return lambdaForTopPercentValues;
    }

    private long convertPercentToLimit(double percent, int collectionSize) {
        return (long) (collectionSize * percent);
    }

}
