package com.example.implementations;

import com.example.interfaces.IBigDecimalDeserializer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BigDecimalDeserializer implements IBigDecimalDeserializer {

    @Override
    public List<BigDecimal> deserialize(String filename) throws IOException, ClassNotFoundException {

        FileInputStream fileInputStream = new FileInputStream(filename);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

        List<BigDecimal> deserializationResult = new ArrayList<>();

        while (isStreamNonEmpty(fileInputStream)) {
            BigDecimal current = (BigDecimal) objectInputStream.readObject();
            deserializationResult.add(current);
        }

        objectInputStream.close();

        return deserializationResult;
    }

    private boolean isStreamNonEmpty(FileInputStream fileInputStream) throws IOException {
        return fileInputStream.available() > 0;
    }
}
