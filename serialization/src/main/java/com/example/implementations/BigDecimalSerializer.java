package com.example.implementations;

import com.example.exceptions.SerializationException;
import com.example.interfaces.IBigDecimalSerializer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

public class BigDecimalSerializer implements IBigDecimalSerializer {

    @Override
    public void serialize(List<BigDecimal> values, String filename) throws IOException {

        FileOutputStream fileOutputStream = new FileOutputStream(filename);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

        serializeStream(values.stream(), objectOutputStream);
    }

    private void serializeStream(Stream<BigDecimal> stream, ObjectOutputStream objectOutputStream) {
        stream.forEach(value -> {
            try {
                objectOutputStream.writeObject(value);
                objectOutputStream.flush();
            } catch (IOException writingException) {
                try {
                    objectOutputStream.close();
                    throw new SerializationException(
                            "Serializing big decimals failed: " + writingException.getMessage()
                    );
                } catch (IOException closingException) {
                    throw new SerializationException(
                            "Closing object output stream failed: " + closingException.getMessage()
                    );
                }
            }
        });

        try {
            objectOutputStream.close();
        } catch (IOException closingException) {
            throw new SerializationException(
                    "Closing object output stream failed: " + closingException.getMessage()
            );
        }
    }
}
