package com.example.implementations;

import com.example.interfaces.ILambdaDeserializer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class LambdaDeserializer implements ILambdaDeserializer {

    @Override
    public Runnable deserialize(String filename) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(filename);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

        Runnable lambda = (Runnable) objectInputStream.readObject();
        objectInputStream.close();

        return lambda;
    }
}
