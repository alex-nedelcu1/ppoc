package com.example.implementations;

import com.example.interfaces.ILambdaSerializer;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class LambdaSerializer implements ILambdaSerializer {

    @Override
    public void serialize(Runnable lambda, String filename) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(filename);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

        objectOutputStream.writeObject(lambda);
        objectOutputStream.flush();
        objectOutputStream.close();
    }
}
