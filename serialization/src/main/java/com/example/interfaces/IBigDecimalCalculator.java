package com.example.interfaces;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public interface IBigDecimalCalculator extends Serializable {

    BigDecimal getSum(List<BigDecimal> values);

    BigDecimal getAverage(List<BigDecimal> values);

    List<BigDecimal> getTopPercentValues(List<BigDecimal> values, double percent);

    Runnable getLambdaForSum(List<BigDecimal> values);

    Runnable getLambdaForAverage(List<BigDecimal> values);

    Runnable getLambdaForTopPercentValues(List<BigDecimal> values, double percent);
}
