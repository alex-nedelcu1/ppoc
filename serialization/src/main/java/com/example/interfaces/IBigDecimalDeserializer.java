package com.example.interfaces;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public interface IBigDecimalDeserializer {

    List<BigDecimal> deserialize(String filename) throws IOException, ClassNotFoundException;

}
