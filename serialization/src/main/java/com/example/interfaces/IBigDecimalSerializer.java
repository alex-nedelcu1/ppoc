package com.example.interfaces;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public interface IBigDecimalSerializer {

    void serialize(List<BigDecimal> values, String filename) throws IOException;

}
