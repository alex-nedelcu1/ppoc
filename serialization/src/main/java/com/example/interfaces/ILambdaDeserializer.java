package com.example.interfaces;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public interface ILambdaDeserializer {

    Runnable deserialize(String filename) throws IOException, ClassNotFoundException;

}
