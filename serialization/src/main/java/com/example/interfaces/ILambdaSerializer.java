package com.example.interfaces;

import java.io.IOException;

public interface ILambdaSerializer {

    void serialize(Runnable lambda, String filename) throws IOException;

}
