package com.example;

import com.example.implementations.BigDecimalCalculator;
import com.example.interfaces.IBigDecimalCalculator;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BigDecimalCalculatorTest {

    private final IBigDecimalCalculator bigDecimalCalculator = new BigDecimalCalculator();

    @Test
    public void shouldComputeSumForNonEmptyArray() {
        int exclusiveUpperBound = 3_000_000;

        BigDecimal expectedSum = BigDecimal.valueOf(exclusiveUpperBound - 1)
                .multiply(BigDecimal.valueOf(exclusiveUpperBound))
                .divide(BigDecimal.valueOf(2), RoundingMode.UNNECESSARY);

        List<BigDecimal> values = IntStream.range(0, exclusiveUpperBound)
                .mapToObj(BigDecimal::new)
                .collect(Collectors.toList());

        assertEquals(bigDecimalCalculator.getSum(values), expectedSum);
    }

    @Test
    public void shouldReturnSumZeroForEmptyArray() {
        List<BigDecimal> values = new ArrayList<>();
        assertEquals(bigDecimalCalculator.getSum(values), BigDecimal.ZERO);
    }

    @Test
    public void shouldComputeAverageForNonEmptyArray() {
        int exclusiveUpperBound = 100_000;

        BigDecimal sum = BigDecimal.valueOf(exclusiveUpperBound - 1)
                .multiply(BigDecimal.valueOf(exclusiveUpperBound))
                .divide(BigDecimal.valueOf(2), RoundingMode.UNNECESSARY);

        BigDecimal howMany = BigDecimal.valueOf(exclusiveUpperBound);

        BigDecimal expectedAverage = sum.divide(howMany);

        List<BigDecimal> values = IntStream.range(0, exclusiveUpperBound)
                .mapToObj(BigDecimal::new)
                .collect(Collectors.toList());

        assertEquals(bigDecimalCalculator.getAverage(values), expectedAverage);
    }

    @Test
    public void shouldReturnAverageNullForEmptyArray() {
        List<BigDecimal> values = new ArrayList<>();
        assertNull(bigDecimalCalculator.getAverage(values));
    }

    @Test
    public void shouldGetFirstValuesByPercent() {
        int exclusiveUpperBound = 11;

        List<BigDecimal> values = IntStream.range(0, exclusiveUpperBound)
                .mapToObj(BigDecimal::new)
                .collect(Collectors.toList());

        List<BigDecimal> expectedResult = new ArrayList<>();
        expectedResult.add(BigDecimal.valueOf(10));
        expectedResult.add(BigDecimal.valueOf(9));

        assertEquals(
                bigDecimalCalculator.getTopPercentValues(values, 0.2),
                expectedResult
        );
    }
}
