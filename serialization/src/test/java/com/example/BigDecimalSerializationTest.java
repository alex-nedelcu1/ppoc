package com.example;

import com.example.implementations.BigDecimalDeserializer;
import com.example.implementations.BigDecimalSerializer;
import com.example.interfaces.IBigDecimalDeserializer;
import com.example.interfaces.IBigDecimalSerializer;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

public class BigDecimalSerializationTest {

    private final IBigDecimalSerializer bigDecimalSerializer = new BigDecimalSerializer();
    private final IBigDecimalDeserializer bigDecimalDeserializer = new BigDecimalDeserializer();

    @Test
    public void shouldSerializeAndDeserializeBigDecimalsArray() {
        int exclusiveUpperBound = 10_000;

        List<BigDecimal> values = IntStream.range(0, exclusiveUpperBound)
                .mapToObj(BigDecimal::new)
                .collect(Collectors.toList());

        String serializationFilename = "values.ser";

        try {
            bigDecimalSerializer.serialize(values, serializationFilename);
            List<BigDecimal> deserializationResult = bigDecimalDeserializer.deserialize(serializationFilename);

            assertEquals(deserializationResult.size(), values.size());
            assertEquals(deserializationResult, values);
        } catch (IOException | ClassNotFoundException exception) {
            throw new AssertionError(exception.getMessage());
        }
    }
}
