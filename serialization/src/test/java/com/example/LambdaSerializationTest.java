package com.example;

import com.example.implementations.BigDecimalCalculator;
import com.example.implementations.LambdaDeserializer;
import com.example.implementations.LambdaSerializer;
import com.example.interfaces.IBigDecimalCalculator;
import com.example.interfaces.ILambdaDeserializer;
import com.example.interfaces.ILambdaSerializer;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertNotNull;

public class LambdaSerializationTest {

    private final ILambdaSerializer lambdaSerializer = new LambdaSerializer();
    private final ILambdaDeserializer lambdaDeserializer = new LambdaDeserializer();
    private final IBigDecimalCalculator bigDecimalCalculator = new BigDecimalCalculator();

    @Test
    public void shouldSerializeAndDeserializeGetSumLambda() {
        int exclusiveUpperBound = 3;

        List<BigDecimal> values = IntStream.range(0, exclusiveUpperBound)
                .mapToObj(BigDecimal::new)
                .collect(Collectors.toList());

        String serializationFilename = "lambdas.ser";

        try {
            lambdaSerializer.serialize(bigDecimalCalculator.getLambdaForSum(values), serializationFilename);
            Runnable deserializedLambdaForSum = lambdaDeserializer.deserialize(serializationFilename);
            assertNotNull(deserializedLambdaForSum);

            System.out.print("Result of running deserialized get sum lambda: ");
            deserializedLambdaForSum.run();
        } catch (IOException | ClassNotFoundException exception) {
            throw new AssertionError(exception.getMessage());
        }
    }

    @Test
    public void shouldSerializeAndDeserializeGetAverageLambda() {
        int exclusiveUpperBound = 10;

        List<BigDecimal> values = IntStream.range(0, exclusiveUpperBound)
                .mapToObj(BigDecimal::new)
                .collect(Collectors.toList());

        String serializationFilename = "lambdas.ser";

        try {
            lambdaSerializer.serialize(bigDecimalCalculator.getLambdaForAverage(values), serializationFilename);
            Runnable deserializedLambdaForSum = lambdaDeserializer.deserialize(serializationFilename);
            assertNotNull(deserializedLambdaForSum);

            System.out.print("Result of running deserialized get average lambda: ");
            deserializedLambdaForSum.run();
        } catch (IOException | ClassNotFoundException exception) {
            throw new AssertionError(exception.getMessage());
        }
    }

    @Test
    public void shouldSerializeAndDeserializeGetTopPercentValuesLambda() {
        int exclusiveUpperBound = 10;

        List<BigDecimal> values = IntStream.range(0, exclusiveUpperBound)
                .mapToObj(BigDecimal::new)
                .collect(Collectors.toList());

        String serializationFilename = "lambdas.ser";

        try {
            lambdaSerializer.serialize(bigDecimalCalculator.getLambdaForTopPercentValues(values, 0.2), serializationFilename);
            Runnable deserializedLambdaForSum = lambdaDeserializer.deserialize(serializationFilename);
            assertNotNull(deserializedLambdaForSum);

            System.out.print("Result of running deserialized get top percent values lambda: ");
            deserializedLambdaForSum.run();
        } catch (IOException | ClassNotFoundException exception) {
            throw new AssertionError(exception.getMessage());
        }
    }
}
